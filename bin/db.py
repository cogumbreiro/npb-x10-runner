#!/usr/bin/env python
#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt
import json
import os
from os import path

def _load_entry(benchmark, version, nthreads, data_dir):
    filename = "%s/%s-%02d.json" % (version, benchmark, nthreads)
    with open(path.join(data_dir, filename)) as fp:
        data = json.load(fp)
        return {'mean': data['mean'],
                'margin_error': data['margin_error'],
                'nthreads': nthreads}
    
ORIG = 'origin'
ARMUS = 'checked'
VERSIONS = (ARMUS, ORIG)

def _load_benchmark(benchmark, nthreads, data_dir):
    for nt in nthreads:
        filename = "%s-%02d.json" % (benchmark, nt)
        with open(path.join(data_dir, filename)) as fp:
            data = json.load(fp)
            orig = _load_entry(benchmark, ORIG, nt, data_dir)
            armus = _load_entry(benchmark, ARMUS, nt, data_dir)
            yield {'nthreads': nthreads,
                    'mean': data['mean'],
                    'margin_error': data['margin_error'],
                    'comparable': data['comparable'],
                    'ratio': data['ratio'],
                    'origin': orig,
                    'checked': armus}

def load_benchmark(benchmark, nthreads, data_dir):
    return list(_load_benchmark(benchmark, nthreads, data_dir))
