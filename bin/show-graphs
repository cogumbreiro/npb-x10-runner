#!/usr/bin/env python
#!/usr/bin/env python
# a bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt
import json
import db
from npb import *

def main(benchmark, nthreads):
    data = db.load_benchmark(benchmark, nthreads, "data")
    armus_data = tuple(x[db.ARMUS] for x in data)
    orig_data = tuple(x[db.ORIG] for x in data)
    N = len(data)
    armus_means = tuple(x['mean'] for x in armus_data)
    men_errs = tuple(x['margin_error'] for x in armus_data)
    orig_means = tuple(x['mean'] for x in orig_data)
    orig_errs = tuple(x['margin_error'] for x in orig_data)
    labels = tuple(x['nthreads'] for x in armus_data)
    slowdowns = tuple(int(x['ratio'] * 100) for x in data)


    ind = np.arange(N)  # the x locations for the groups
    width = 0.35       # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, armus_means, width, color='r', yerr=men_errs)
    # overlap both bars
    rects2 = ax.bar(ind, orig_means, width, color='y', yerr=orig_errs)

    # add some
    ax.set_ylabel('Time (s)')
    ax.set_title(benchmark)
    ax.set_xticks(ind+width)
    ax.set_xticklabels(labels)

    ax.legend( (rects1[0], rects2[0]), ('Armus', 'Original'), 'upper left')

    def autolabel(rects):
        # attach some text labels
        for rect, idx in zip(rects, range(N)):
            height = rect.get_height()
            if data[idx]['comparable']:
                lbl = '%d%%' % slowdowns[idx]
            else:
                lbl = 'n/a'
            ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, lbl,
                    ha='center', va='bottom')

    autolabel(rects1)

    plt.show()
if __name__ == '__main__':
    from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
    parser = ArgumentParser(description='Show a graph for the benchmark.', formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('prog', metavar="PROGRAM", choices=PROGS, help="The name of the benchmark to run.")
    parser.add_argument('nthreads', nargs='+', help='The number of threads to show.', default=(1,), type=int)
    r = parser.parse_args()
    main(r.prog, r.nthreads)
