
REPO = https://bitbucket.org/cogumbreiro/npb3.0-x10/
ORIG = origin
CHECK = checked

all: compile

run: init
	./benchmarktk/run-benchmark setup.yaml

collect: init
	./benchmarktk/collect-data setup.yaml

pull: update

up: update

update: init
	git pull
	(cd armus-x10; git pull)
	(cd $(CHECK); git pull)
	(cd $(ORIG); git pull)
	(cd stats-tk; git pull)
	(cd benchmarktk; git pull)

compile: init
	(cd armus-x10; ant)
	(cp armus-x10/target/armusc-x10.jar $(CHECK)/)
	(cp armus-x10/target/armus-rt-x10.jar $(CHECK)/)
	(cd $(CHECK); make)
	(cd $(ORIG); make)

init: armus-x10 $(CHECK) $(ORIG) stats-tk benchmarktk \
	data/$(ORIG) data/$(CHECK) log/$(ORIG) log/$(CHECK)

data/$(ORIG):
	mkdir -p $@

data/$(CHECK):
	mkdir -p $@

log/$(ORIG):
	mkdir -p $@

log/$(CHECK):
	mkdir -p $@

armus-x10:
	git clone https://bitbucket.org/cogumbreiro/armus-x10/

$(CHECK):
	git clone $(REPO) $(CHECK)
	(cd $(CHECK); git checkout armus-x10)

$(ORIG):
	git clone $(REPO) $(ORIG)

stats-tk:
	git clone https://bitbucket.org/cogumbreiro/stats-tk/ stats-tk

benchmarktk:
	git clone https://bitbucket.org/cogumbreiro/benchmarktk

clean:
	(cd armus-x10; ant clean)
	(cd $(CHECK); make clean)
	(cd $(ORIG); make clean)
